﻿using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Services.Client;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.SharePoint.Client;
using OnCall.Models;
using System.Net.Http.Headers;
using OnCall.OnCall2016SVC;
using EROncallScheduleFacilityValue = OnCall.Models.EROncallScheduleFacilityValue;

namespace OnCall.Controllers
{
    [RoutePrefix("api/oncall")]
    public class OnCallController : ApiController
    {
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private static readonly string OnCallUrl = ConfigurationManager.AppSettings["OnCallUrl"].ToString();
        private static readonly string OnCallPDFUrl = ConfigurationManager.AppSettings["OnCallPDFUrl"].ToString();
        private static readonly string EROnCallGuid = ConfigurationManager.AppSettings["EROnCallGuid"].ToString();

        public OnCallController()
        {
            var config = new NLog.Config.LoggingConfiguration();
            var logFile = new NLog.Targets.FileTarget("logfile")
                {FileName = $"Logs\\OnCallMicroservice{DateTime.Today:MM-dd-yy}.log"};
            config.AddRule(LogLevel.Info, LogLevel.Fatal, logFile);
            NLog.LogManager.Configuration = config;
        }

        // Original path in PPAPI EROnCall2016ScheduleList
        [HttpGet]
        [Route("EROnCallScheduleList")]
        public IHttpActionResult GetEROnCallScheduleList()
        {
            var facilityList = new FacilityList
            {
                Facilities = new List<Facility>()
            };

            try
            {
                using (var ctx = new ClientContext(OnCallUrl))
                {
                    ctx.Credentials = new NetworkCredential("wedadmsvc", "w3d4DM$vC", "bh");
                    var list = ctx.Web.Lists.GetById(new Guid(EROnCallGuid));

                    var view = list.Views.GetByTitle("All Documents");
                    ctx.Load(view);

                    var query = new CamlQuery();
                    if (ctx.HasPendingRequest)
                    {
                        ctx.ExecuteQuery();
                    }

                    query.ViewXml = $"<View><Query>{view.ViewQuery}{query}</Query></View>";

                    var items = list.GetItems(query);

                    ctx.Load(items);

                    if (ctx.HasPendingRequest)
                    {
                        ctx.ExecuteQuery();
                    }

                    foreach (var item in items)
                    {
                        var sch = new EROnCallSchedule
                        {
                            ID = item.Id,
                            Title = item["FileLeafRef"].ToString(),
                            Specialty = item["Specialty"].ToString()
                        };

                        var facilities = (string[])item["Facility"];

                        if (facilities == null) 
                            continue;

                        foreach (var facility in facilities)
                        {
                            var fac = new Facility
                            {
                                ErOnCallSchedule = new List<EROnCallSchedule>(),
                                FacilityName = facility.ToString()
                            };
                            fac.ErOnCallSchedule.Add(sch);

                            facilityList.Facilities.Add(fac);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error("Exception occurred getting EROnCallScheduleList :: " + e.Message);
                return BadRequest();
            }
            return Ok(facilityList);
            }

        [HttpPost]
        [Route("EROnCallSchedulesItemFilesPDFByID")]
        public IHttpActionResult GetEROnCallSchedulesItemFilesPDFByID()
        {
            try
            {
                var pp = new MedicalStaffOfficeDataContext(new Uri(OnCallPDFUrl))
                {
                    Credentials = CredentialCache.DefaultCredentials
                };
                var doc = new EROncallScheduleItem();
                var documentList = new List<EROncallScheduleItem>();
                try
                {
                    var headers = Request.Headers;
                    string linkId;
                    documentList = pp.EROncallSchedule.ToList();

                    if (headers.Contains("ID"))
                    {
                        linkId = headers.GetValues("ID").First();
                        doc = (from x in documentList where x.Id.ToString() == linkId select x).First();
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("Exception occurred getting id/object  from EROnCallSchedulesItemFilesPDFByID :: " + ex.Message);
                    return BadRequest();
                }

                var dssr = pp.GetReadStream(doc);
                var stream = dssr.Stream;

                var result = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StreamContent(stream)
                };

                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = doc.Name
                };

                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                var response = ResponseMessage(result);
                return response;
            }
            catch (Exception e)
            {
                Logger.Error("Exception occurred getting EROnCallSchedulesItemFilesPDFByID :: " + e.Message);
                return BadRequest();
            }
        }
    }
}


