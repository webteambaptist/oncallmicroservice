﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnCall.Models
{
    public class FacilityList
    {
        public List<Facility> Facilities { get; set; }
    }
    public class Facility
    {
        public string FacilityName { get; set; }
        public List<EROnCallSchedule> ErOnCallSchedule { get; set; }
    }

    public class EROncallScheduleFacilityValue
    {
        public string Value { get; set; }
    }
}